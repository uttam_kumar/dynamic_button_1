package com.example.uttam.dynamicbutton;

import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //private Button btn1,btn2,btn3,btn4,btn5;

    private int n;
    private Button btnTag;
    private Button clickedButton;
    private int p=0,q=10000;
    private ImageView microphone;
    private Handler handler;
    //private GridView grid;
    private Button[] btnArr;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //grid=findViewById(R.id.btnGridViewId);
        microphone=findViewById(R.id.microphoneImageViewId);

        Random rand = new Random();
        n = rand.nextInt((40 - 5) + 1) + 5;
        Log.d("btn", "Clicked Button Number: "+n);

        microphoneVisible(n);

        LinearLayout layout = (LinearLayout) findViewById(R.id.btnLayout);
        layout.setPadding(10,0,10,0);
        layout.setGravity(1);

        //grid = (GridView) findViewById(R.id.btnGridViewId);
        btnArr=new Button[n];
        for(int i=0;i<n;i++) {

            btnTag = new Button(this);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(200,85);
            params.setMargins(25,10,25,0);

            btnTag.setText("Button_"+(i+1));
            int textLength=btnTag.length();

            if(textLength > 8 && textLength < 12){
                btnTag.setTextSize(12);
            }else if(textLength >= 13){
                btnTag.setTextSize(10);
            }else{
                btnTag.setTextSize(16);
            }

            //btnTag.setTextSize(8);

            btnTag.setPadding(10,0,10,0);
            btnTag.setSingleLine(true);
            btnTag.setEllipsize(TextUtils.TruncateAt.END);
            btnTag.setId(i+1);
            btnTag.setBackgroundResource(R.drawable.button_shape);
            btnTag.setTextColor(getResources().getColor(R.color.btnTextColor));

            btnTag.setWidth(200);
            btnTag.setHeight(85);
            //btnArr[i]=btnTag;

            //add button to the layout
            layout.addView(btnTag,params);
            //layout.addView(btnTag);
            btnTag.setOnClickListener(this);
        }

//        final List<Button> plantsList = new ArrayList<Button>(Arrays.asList(btnArr));
//
//        // Create a new ArrayAdapter
//        final ArrayAdapter<Button> gridViewArrayAdapter = new ArrayAdapter<Button>
//                (this,android.R.layout.simple_list_item_1, plantsList);
//
//        // Data bind GridView with ArrayAdapter (String Array elements)
//        grid.setAdapter(gridViewArrayAdapter);

    }

    @Override
    public void onClick(View v) {

        if(p>0){
            //clickedButton.setBackgroundResource(R.color.colorAccent);
            clickedButton.setBackgroundResource(R.drawable.button_shape);
        }

        p=v.getId();
        clickedButton=findViewById(p);

        for(int y=0;y<n;y++){
            if(y+1==p){
                clickedButton.setBackgroundResource(R.drawable.button_shape_click);
                Toast.makeText(getApplicationContext(), "Clicked button_Id: "+p, Toast.LENGTH_SHORT).show();
            }
        }
        p++;
    }


    public void microphoneVisible(int r){
        Log.d("TAG", "microphoneVisible: "+r);

        if(r%2==0) {
            microphone.setVisibility(View.VISIBLE);
            microphone.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    microphone.setClickable(false);
                    Glide.with(getApplicationContext()).load(R.drawable.input_m).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).crossFade().into(microphone);

                    if(p>0){
                        clickedButton.setBackgroundResource(R.drawable.button_shape);
                    }

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            // Actions to do after 10 seconds
                            microphone.setImageResource(R.drawable.microphone);
                            microphone.setClickable(true);
                        }
                    }, 10000);
                }
            });

        }else{

            microphone.setVisibility(View.GONE);
        }
    }

}
